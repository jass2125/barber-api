package br.com.barber.api.services.servicos;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.barber.api.exceptions.ServicoNaoEncontradoException;
import br.com.barber.api.model.Servico;
import br.com.barber.api.repositories.servicos.ServicoRepository;

@Service
public class ServicoServiceImpl implements ServicoService {
	
	@Autowired
	private ServicoRepository servicoRepository;
	
	@Transactional(propagation = Propagation.REQUIRED)
	public void salvar(Servico servico) {
		servicoRepository.salvar(servico);
	}
	
	public Servico buscarPorId(Long id) {
		Servico servico = servicoRepository.buscarPorId(id);
		if(servico == null) {
			throw new ServicoNaoEncontradoException("Não existe serviço com esse ID: " + id);
		}
		return servico;
	}
	
	public List<Servico> buscarTodos() {
		return servicoRepository.buscarTodos();
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public Servico atualizar(Servico servico) {
		this.servicoRepository.buscarPorId(servico.getId());
		return servicoRepository.atualizar(servico);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void excluir(Long id) {
		Servico servico = buscarPorId(id);
		this.servicoRepository.excluir(servico);
	}
	
}

package br.com.barber.api.services.servicos;

import java.util.List;

import br.com.barber.api.model.Servico;

public interface ServicoService {
	
	public void salvar(Servico servico);
	
	public Servico buscarPorId(Long id);
	
	public List<Servico> buscarTodos();
	
	public Servico atualizar(Servico servico);

	public void excluir(Long id);
	
}

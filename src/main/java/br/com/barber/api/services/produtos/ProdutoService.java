package br.com.barber.api.services.produtos;

import java.util.List;

import br.com.barber.api.model.Produto;

public interface ProdutoService {

	public void salvar(Produto produto);
	
	public Produto buscarPorId(Long id);
	
	public List<Produto> buscarTodos();
	
	public Produto atualizar(Produto produto);

	public void excluir(Long id);

	
}

package br.com.barber.api.services.produtos;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.barber.api.exceptions.ProdutoNaoEncontradoException;
import br.com.barber.api.model.Produto;
import br.com.barber.api.repositories.produtos.ProdutoRepository;

@Service
public class ProdutoServiceImpl implements ProdutoService {

	@Autowired
	private ProdutoRepository produtoRepository;
	
	@Transactional(propagation = Propagation.REQUIRED)
	public void salvar(Produto produto) {
		produtoRepository.salvar(produto);
	}
	
	public Produto buscarPorId(Long id) {
		Produto produto = produtoRepository.buscarPorId(id);
		if(produto == null) {
			throw new ProdutoNaoEncontradoException("Não existe produto com esse ID: " + id);
		}
		return produto;
	}
	
	public List<Produto> buscarTodos() {
		return produtoRepository.buscarTodos();
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public Produto atualizar(Produto produto) {
		this.produtoRepository.buscarPorId(produto.getId());
		return produtoRepository.atualizar(produto);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void excluir(Long id) {
		Produto produto = buscarPorId(id);
		this.produtoRepository.excluir(produto);
	}

}

package br.com.barber.api.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CPF;

@Embeddable
public class DocumentoPessoal {

	@NotNull
	@NotEmpty
	@Column(name = "rg", length = 30)
	private String rg;
	@NotNull
	@NotEmpty
	@CPF
	@Column(name = "cpf", length = 20)
	private String cpf;
	
	public DocumentoPessoal() {
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	@Override
	public String toString() {
		return "DocumentoPessoal [rg=" + rg + ", cpf=" + cpf + "]";
	}
	
}

package br.com.barber.api.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Embeddable
public class Endereco {

	@NotNull
	@NotBlank
	@Column(name = "rua")
	private String rua;
	@NotNull
	@NotBlank
	@Column(name = "bairro")
	private String bairro;
	@NotNull
	@NotBlank
	@Column(name = "numero")
	private String numero;
	@NotNull
	@NotBlank
	@Column(name = "cep")
	private String cep;
	@NotNull
	@NotBlank
	@Column(name = "cidade")
	private String cidade;
	@NotNull
	@NotBlank
	@Column(name = "estado")
	private String estado;
	
	public Endereco() {
	}

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "Endereco [rua=" + rua + ", bairro=" + bairro + ", numero=" + numero + ", cep=" + cep + ", cidade="
				+ cidade + ", estado=" + estado + "]";
	}
	
}

package br.com.barber.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity	
@Table(name = "t_servico")
@SequenceGenerator(name = "servico_seq", sequenceName = "servico_seq", allocationSize = 1, initialValue = 1)
public class Servico implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "servico_seq")
	private Long id;
	@NotEmpty(message = "{servico.descricao.not.empty}")
	@NotNull(message = "{servico.descricao.not.null}")
	private String descricao;
	@NotNull
	private LocalTime tempoEstimado;
	@NotNull
	@Column(precision= 10, scale = 2, name = "preco", columnDefinition = "numeric(10, 2)")
	private BigDecimal preco;
	@ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
	private Administrador administrador;
	
	public Servico() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public LocalTime getTempoEstimado() {
		return tempoEstimado;
	}

	public void setTempoEstimado(LocalTime tempoEstimado) {
		this.tempoEstimado = tempoEstimado;
	}

	public BigDecimal getPreco() {
		return preco;
	}

	public void setPreco(BigDecimal preco) {
		this.preco = preco;
	}

	public Administrador getAdministrador() {
		return administrador;
	}

	public void setAdministrador(Administrador administrador) {
		this.administrador = administrador;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Servico other = (Servico) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Servico [id=" + id + ", descricao=" + descricao + ", tempoEstimado=" + tempoEstimado + ", preco="
				+ preco + ", administrador=" + administrador + "]";
	}
	
}

package br.com.barber.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity	
@Table(name = "t_usuario")
@SequenceGenerator(name = "usuario_seq", sequenceName = "usuario_seq")
public class Usuario implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usuario_seq")
	private Long id;
	@NotNull
	@NotEmpty
	@Column(name = "nome", length = 200)
	private String nome;
	@Embedded
	@Valid
	private DocumentoPessoal documentoPessoal;
	@Embedded
	@Valid
	private Conta conta;
	@Valid
	@Embedded
	private Contato contato;
	@NotNull
	@NotEmpty
	@Enumerated(EnumType.STRING)
	@Column(name = "status", length = 30)
	private StatusPerfil status;
	
	public Usuario() {
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public DocumentoPessoal getDocumentoPessoal() {
		return documentoPessoal;
	}

	public void setDocumentoPessoal(DocumentoPessoal documentoPessoal) {
		this.documentoPessoal = documentoPessoal;
	}

	public Contato getContato() {
		return contato;
	}

	public void setContato(Contato contato) {
		this.contato = contato;
	}
	
	
	public StatusPerfil getStatus() {
		return status;
	}

	public void setStatus(StatusPerfil status) {
		this.status = status;
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Usuario [id=" + id + ", nome=" + nome + ", documentoPessoal=" + documentoPessoal + ", contato="
				+ contato + ", status=" + status + "]";
	}

}

package br.com.barber.api.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class Promocao {
	
	@NotNull
	@NotBlank
	@FutureOrPresent
	@Column(name = "descricao")
	private LocalDate dataInicial;
	private LocalDate dataFinal;
	
	
	public Promocao() {
	}


	public LocalDate getDataInicial() {
		return dataInicial;
	}


	public void setDataInicial(LocalDate dataInicial) {
		this.dataInicial = dataInicial;
	}


	public LocalDate getDataFinal() {
		return dataFinal;
	}


	public void setDataFinal(LocalDate dataFinal) {
		this.dataFinal = dataFinal;
	}
	
	
}

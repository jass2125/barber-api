package br.com.barber.api.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Embeddable
public class Conta {
	
	@NotNull
	@NotEmpty
	@Email
	@Column(name = "email", length = 30)
	private String email;
	@NotNull
	@NotEmpty
	@Column(name = "senha", length = 200)
	private String senha;
	
	public Conta() {
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getSenha() {
		return senha;
	}
	
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	@Override
	public String toString() {
		return "Conta [email=" + email + ", senha=" + senha + "]";
	}	
	
}

package br.com.barber.api.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Embeddable
public class Contato {
	
	@NotNull
	@NotEmpty
	@Column(name = "telefone", length = 30)
	private String telefone;
	@NotNull
	@NotEmpty
	@Column(name = "celular", length = 30)
	private String celular;	

	public Contato() {
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	@Override
	public String toString() {
		return "Contato [telefone=" + telefone + ", celular=" + celular + "]";
	}
}

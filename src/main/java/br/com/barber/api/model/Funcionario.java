package br.com.barber.api.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "t_funcionario")
@SequenceGenerator(name = "funcionario_seq", sequenceName = "funcionario_seq")
public class Funcionario implements Serializable {
	
	private static final long serialVersionUID = -1935501160212590591L;

	@Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "funcionario_seq")
	private Long id;
	@OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
	private Usuario usuario;
	@Column(name = "cargo", length = 50)
	@NotEmpty
	@NotNull
	private String cargo;
	
	
	public Funcionario() {
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Usuario getUsuario() {
		return usuario;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}


	public String getCargo() {
		return cargo;
	}


	public void setCargo(String cargo) {
		this.cargo = cargo;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcionario other = (Funcionario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
}

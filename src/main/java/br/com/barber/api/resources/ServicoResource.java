package br.com.barber.api.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.barber.api.model.Servico;
import br.com.barber.api.services.servicos.ServicoService;

@RestController
@RequestMapping("/servicos")
public class ServicoResource {
		
	@Autowired
	private ServicoService servicoService;
	
	@PostMapping
	public ResponseEntity<?> save(@RequestBody @Valid Servico servico) {
		this.servicoService.salvar(servico);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Servico> buscarPorId(@PathVariable Long id) {
		Servico servico = this.servicoService.buscarPorId(id);
		return ResponseEntity.ok(servico);
	}
	
	@GetMapping()
	public ResponseEntity<List<Servico>> buscar() {
		List<Servico> listaDeServicos = this.servicoService.buscarTodos();
		return ResponseEntity.ok(listaDeServicos);
	}
	
	@PutMapping
	public ResponseEntity<?> atualizar(@RequestBody Servico servico) {
		this.servicoService.atualizar(servico);
		return ResponseEntity.ok().build();
	}
	

	@DeleteMapping("/{id}")
	public ResponseEntity<?> excluir(@PathVariable Long id) {
		this.servicoService.excluir(id);
		return ResponseEntity.ok().build();
	}
	
}

package br.com.barber.api.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.barber.api.model.Produto;
import br.com.barber.api.services.produtos.ProdutoService;

@RestController
@RequestMapping("/produtos")
public class ProdutoResource {
		
	@Autowired
	private ProdutoService produtoService;
	
	@PostMapping
	public ResponseEntity<?> save(@RequestBody @Valid Produto produto) {
		this.produtoService.salvar(produto);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Produto> buscarPorId(@PathVariable Long id) {
		Produto produto = this.produtoService.buscarPorId(id);
		return ResponseEntity.ok(produto);
	}
	
	@GetMapping()
	public ResponseEntity<List<Produto>> buscar() {
		List<Produto> listaDeServicos = this.produtoService.buscarTodos();
		return ResponseEntity.ok(listaDeServicos);
	}
	
	@PutMapping
	public ResponseEntity<?> atualizar(@RequestBody Produto produto) {
		this.produtoService.atualizar(produto);
		return ResponseEntity.ok().build();
	}
	

	@DeleteMapping("/{id}")
	public ResponseEntity<?> excluir(@PathVariable Long id) {
		this.produtoService.excluir(id);
		return ResponseEntity.ok().build();
	}
	
}

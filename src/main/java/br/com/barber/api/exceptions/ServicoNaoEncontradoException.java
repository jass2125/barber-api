package br.com.barber.api.exceptions;

public class ServicoNaoEncontradoException extends RuntimeException {

	
	private static final long serialVersionUID = 4049655607922664339L;
	
	public ServicoNaoEncontradoException(String mensagem) {
		super(mensagem);
	}

	
}

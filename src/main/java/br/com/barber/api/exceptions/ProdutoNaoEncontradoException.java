package br.com.barber.api.exceptions;

public class ProdutoNaoEncontradoException extends RuntimeException {

	

	private static final long serialVersionUID = -5076448920080940507L;
	
	public ProdutoNaoEncontradoException(String mensagem) {
		super(mensagem);
	}
		
}

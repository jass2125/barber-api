package br.com.barber.api.exceptions;

import java.time.LocalDate;
import java.util.List;

import org.springframework.http.HttpStatus;

public class ErroException {
	
	private LocalDate horario;
	private HttpStatus status;
	private String mensagem;
	private List<String> detalhes;
	
	public ErroException() {
	}

	public ErroException(LocalDate horario, HttpStatus status, String mensagem, List<String> detalhes) {
		super();
		this.horario = horario;
		this.status = status;
		this.mensagem = mensagem;
		this.detalhes = detalhes;
	}

	public LocalDate getHorario() {
		return horario;
	}

	public void setHorario(LocalDate horario) {
		this.horario = horario;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public List<String> getDetalhes() {
		return detalhes;
	}

	public void setDetalhes(List<String> detalhes) {
		this.detalhes = detalhes;
	}

	@Override
	public String toString() {
		return "DetalhesException [horario=" + horario + ", status=" + status + ", mensagem=" + mensagem + ", detalhes="
				+ detalhes + "]";
	}
	
}

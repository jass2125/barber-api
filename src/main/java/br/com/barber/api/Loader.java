package br.com.barber.api;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Loader {
	
	
	public static void main(String[] args) {
		SpringApplication.run(Loader.class, args);
		
		
//		ZoneId zona = ZoneId.of("Brazil/East");
//		LocalTime local = LocalTime.now(zona);
//		System.out.println(local);
		
	}

}


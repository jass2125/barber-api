package br.com.barber.api.repositories.produtos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.barber.api.model.Produto;

@Repository
public class ProdutoRepositoryImpl implements ProdutoRepository {
	
	@PersistenceContext
	private EntityManager em;
	
	public void salvar(Produto produto) {
		em.persist(produto);
	}
	
	public Produto buscarPorId(Long id) {
		return em.find(Produto.class, id);
	}
	
	public Produto atualizar(Produto produto) {
		return em.merge(produto);
	}
	
	public void excluir(Produto produto) {
		em.remove(produto);
	}

	@Override
	public List<Produto> buscarTodos() {
		return em.createQuery("SELECT P FROM Produto P", Produto.class).getResultList();
	}
	
}

package br.com.barber.api.repositories.servicos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.barber.api.model.Servico;

@Repository
public class ServicoRepositoryImpl implements ServicoRepository{
	
	@PersistenceContext
	private EntityManager em;
	
	public void salvar(Servico servico) {
		em.persist(servico);
	}
	
	public Servico buscarPorId(Long id) {
		return em.find(Servico.class, id);
	}
	
	public Servico atualizar(Servico servico) {
		return em.merge(servico);
	}
	
	public void excluir(Servico servico) {
		em.remove(servico);
	}

	@Override
	public List<Servico> buscarTodos() {
		return em.createQuery("SELECT S FROM Servico S", Servico.class).getResultList();
	}
}

package br.com.barber.api.repositories.produtos;

import java.util.List;

import br.com.barber.api.model.Produto;

public interface ProdutoRepository {

	public void salvar(Produto produto);
	
	public Produto buscarPorId(Long id);
	
	public Produto atualizar(Produto produto);
	
	public void excluir(Produto produto);

	public List<Produto> buscarTodos();
	
}

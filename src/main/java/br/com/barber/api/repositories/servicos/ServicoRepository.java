package br.com.barber.api.repositories.servicos;

import java.util.List;

import br.com.barber.api.model.Servico;

public interface ServicoRepository {
	
	public void salvar(Servico servico);
	
	public Servico buscarPorId(Long id) ;
	
	public Servico atualizar(Servico servico);
	
	public void excluir(Servico servico);

	public List<Servico> buscarTodos();
	
}
